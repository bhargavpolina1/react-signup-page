import React, { Component } from 'react'
import JoinUsHeading from './JoinUsHeading'
import CenterContainer from './components/CenterContainer'
import './App.css'

class JoinUs extends Component {
  render() {
    return (
      <div className="App">
          <JoinUsHeading/>
          <CenterContainer/>
      </div>
    )
  }
}

export default JoinUs