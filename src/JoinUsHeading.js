import React, { Component } from 'react'
import './heading.css'

export class JoinUsHeading extends Component {
  render() {
    return (
      <h1 className = "headingText">Enter your details below</h1>
    )
  }
}

export default JoinUsHeading