import React, { Component } from "react";
import "../SideContainer.css";
import image from "../sideImageContainer.jpg"

class SideContainer extends Component {
  render() {
    return (
      <div className="sideContainer">
        <img className = "sideContainerImage"
          src={image}
          alt="img"
        />
        <h1 className = "sideHeading">Make The Difference</h1>
        <p>Join Now!</p>
        <a className = "sideAnchor" href = "https://www.mountblue.io/" target="_blank" rel="noreferrer" >Click here to know more about us!</a>
      </div>
    );
  }
}

export default SideContainer;
