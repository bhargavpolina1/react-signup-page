import React, { Component } from "react";
import './LandingPage.css'
class LandingPage extends Component {
  render() {
    return (
      <div className="d-flex flex-column align-items-center mainContainer">
        <h1 className = "mainHeading display-1 mt-5">Join-Learn-Grow</h1>
        <p>Aspiring to grow great in your career?</p>
        <p>Are you looking for a right platform?</p>
        <p>Join us, learn,and land in a high-paying job</p>
      </div>
    );
  }
}

export default LandingPage;
