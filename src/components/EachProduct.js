import React, { Component } from "react";
import "./EachProduct.css";

class EachProduct extends Component {
  constructor(props) {
    super(props);

    const {
      match: { params },
    } = this.props;
    const { id } = params;

    this.state = {
      isProductDataFetched: false,
      FetchedProductData: "",
      idNeeded: id,
      isFetchingFailed: false,
    };
  }

  componentDidMount() {
    fetch(`https://fakestoreapi.com/products/${this.state.idNeeded}`)
      .then((res) => res.json())
      .then((data) => {
        this.setState({
          isProductDataFetched: true,
          FetchedProductData: data,
        });
      })
      .catch((err) => {
        this.setState({
          isFetchingFailed: true,
        });
      });
  }

  render() {
    if (this.state.isProductDataFetched) {
      let lengthOfIdsFetched = Object.keys(
        this.state.FetchedProductData
      ).length;
      if (!lengthOfIdsFetched) {
        return <div>No Product Found</div>;
      } else {
        return (
          <div className="d-flex flex-column align-items-center eachProductMainContainer">
            <div className="d-flex flex-column align-items-center m-1 productIndContainer  bg-white">
              <div className="eachImageContainer">
                <img
                  className="eachProductImage"
                  src={this.state.FetchedProductData.image}
                  alt="img"
                ></img>
              </div>
              <div className="d-flex flex-column p-1 pt-2 justify-content-center align-items-center">
                <h1 className="eachProductName">
                  {this.state.FetchedProductData.title}
                </h1>
                <span className="eachProductCategory">
                  {this.state.FetchedProductData.category}
                </span>
                <br />
                <span className="eachProductDescription">
                  {this.state.FetchedProductData.description}
                </span>
                <br />
                <span className="eachProductPrice">
                  Rs. {this.state.FetchedProductData.price}
                </span>
                <div className="d-flex">
                  <span className="eachProductRate">
                    Rating: {this.state.FetchedProductData.rating.rate}
                  </span>
                  <span className="eachRateCount">
                    Rated By: {this.state.FetchedProductData.rating.count}
                  </span>
                </div>
              </div>
            </div>
          </div>
        )
      }
      }else if (this.state.isFetchingFailed) {
      return (
        <div className="errorContainer">
          <div className="errorImageContainer mt-5">
            <img
              className="errorImage"
              src="https://sitechecker.pro/wp-content/uploads/2017/12/404.png"
              alt="error"
            />
          </div>
          <p>There is an error while fetching data</p>
        </div>
      );
    } else {
      return (
        <div className="spinnerContainer">
          <div className="spinner-border text-primary" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        </div>
      );
    }
  }
}

export default EachProduct;
