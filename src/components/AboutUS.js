import React, { Component } from "react";
import './AboutUS.css'
class AboutUs extends Component {
  render() {
    return(
        <div className = "d-flex flex-column align-items-center aboutMainContainer">
            <p className = "w-50 mt-5 aboutUsContent">We are on a mission to bridge the gap between industry & academia by rapidly building industry-relevant skills in people.For the first time in India, we’ve introduced Industry-Ready Certification (IRC) which represents your industry readiness. We’ve designed programs to help anyone gear up for 4.0 Revolution.</p>
        </div>
        
    ) 
  }
}

export default AboutUs;
