import React, { Component } from "react";
import "./Products.css";

class ProductCard extends Component {
  render() {
    const { title, image, category, price } = this.props.eachProduct;
    return (
      <div className="d-flex flex-column align-items-center m-1 bg-primary productCardContainer">
        <div className="imageContainer">
          <img className="productImage" src={image} alt="img"></img>
        </div>
        <div className = "d-flex flex-column p-1 pt-2 justify-content-center align-items-center">
          <h6 className="productName">{title}</h6>
          <span className="productCategory">{category}</span><br/>
          <span className="productPrice">Rs.{price}</span>
        </div>
      </div>
    );
  }
}

export default ProductCard;
