import React, { Component } from "react";
import FormContent from "./form";
import SideContainer from "./SideContainer";

import "../CenterContainer.css";

class CenterContainer extends Component {
  render() {
    return (
      <div className="centerContainer">
        <FormContent />
        <SideContainer />
      </div>
    );
  }
}

export default CenterContainer;
