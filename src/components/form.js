import React, { Component } from "react";
import "../form.css";

import validator from "validator";

class FormContent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      FirstName: "",
      LastName: "",
      Age: "",
      Gender: "",
      Role: "",
      Email: "",
      Password: "",
      confirmPassword: "",
      CheckBox: "",
      FirstNameError: "",
      LastNameError: "",
      AgeError: "",
      GenderError: "",
      RoleError: "",
      EmailError: "",
      PasswordError: "",
      confirmPasswordError: "",
      TncError: "",
      isDisabled: false,
      FirstNameObtained: false,
      LastNameObtained: false,
      AgeObtained: false,
      GenderObtained: false,
      RoleObtained: false,
      EmailObtained: false,
      PasswordMetRules: false,
      PasswordsMatched: false,
      TermsAccepted: false,
      successMessage: "",
    };
    this.isSubmitted = this.isSubmitted.bind(this);
    this.isBoxChecked = this.isBoxChecked.bind(this);
    this.isChanged = this.isChanged.bind(this);
    this.validateData = this.validateData.bind(this);
    this.validateFields = this.validateFields.bind(this);
    this.getRequiredFields = this.getRequiredFields.bind(this);
  }

  validateData = () => {
    let enteredFirstName = this.state.FirstName.trim();
    let enteredLastName = this.state.LastName.trim();
    let enteredAge = this.state.Age;
    let genderSelected = this.state.Gender;
    let roleSelected = this.state.Role;
    let enteredEmail = this.state.Email;
    let enteredPassword = this.state.Password;
    let enteredConfirmPassword = this.state.confirmPassword;
    let isBoxCheckValue = this.state.CheckBox;

    this.validateFields(
      enteredFirstName,
      enteredLastName,
      enteredAge,
      genderSelected,
      roleSelected,
      enteredEmail,
      enteredPassword,
      enteredConfirmPassword,
      isBoxCheckValue
    );
  };
  validateFields = (
    enteredFirstName,
    enteredLastName,
    enteredAge,
    genderSelected,
    roleSelected,
    enteredEmail,
    enteredPassword,
    enteredConfirmPassword,
    isBoxCheckValue
  ) => {
    let validFirstName = validator.isAlpha(enteredFirstName);

    let stateToUpdate = {};
    if (!validFirstName) {
      stateToUpdate.FirstNameError =
        "Enter a valid first name. It should contain only alphabets";
    } else {
      stateToUpdate.firstName = enteredFirstName;
      stateToUpdate.FirstNameObtained = true;
      stateToUpdate.FirstNameError = "";
    }

    let validLastName = validator.isAlpha(enteredLastName);
    if (!validLastName) {
      stateToUpdate.LastNameError =
        "Enter a valid last name. It should contain only alphabets";
    } else {
      stateToUpdate.LastName = enteredLastName;
      stateToUpdate.LastNameError = "";
      stateToUpdate.astNameObtained = true;
    }

    let isValidAge = validator.isNumeric(enteredAge);
    if (!isValidAge) {
      stateToUpdate.AgeError =
        "Enter a valid age. It should contain only numbers";
    } else {
      stateToUpdate.age = enteredAge;
      stateToUpdate.AgeError = "";
      stateToUpdate.AgeObtained = true;
    }

    let GenderNotSelected =
      genderSelected === "" || genderSelected === "Select Gender";
    if (GenderNotSelected) {
      stateToUpdate.GenderError = "Please select a gender";
    } else {
      stateToUpdate.Gender = genderSelected;
      stateToUpdate.GenderError = "";
      stateToUpdate.GenderObtained = true;
    }

    let roleNotSelected = roleSelected === "" || roleSelected === "Select Role";
    if (roleNotSelected) {
      stateToUpdate.RoleError = "Please select a role";
    } else {
      stateToUpdate.Role = roleSelected;
      stateToUpdate.RoleError = "";
      stateToUpdate.RoleObtained = true;
    }
    let isValidEmail = validator.isEmail(enteredEmail);
    if (!isValidEmail) {
      stateToUpdate.EmailError = "Enter a valid mail id";
    } else {
      stateToUpdate.Email = enteredEmail;
      stateToUpdate.EmailError = "";
      stateToUpdate.EmailObtained = true;
    }

    let isValidPassword = validator.isStrongPassword(enteredPassword);
    if (!isValidPassword) {
      stateToUpdate.PasswordError = "Entered password didn't meet requirement";
    } else {
      stateToUpdate.Password = enteredPassword;
      stateToUpdate.PasswordError = "";
      stateToUpdate.PasswordMetRules = true;
    }

    let didPasswordsMatch =
      enteredPassword !== "" &&
      enteredConfirmPassword !== "" &&
      enteredPassword === enteredConfirmPassword;
    if (!didPasswordsMatch) {
      stateToUpdate.confirmPasswordError =
        "Passwords didn't match. Enter same passwords";
    } else {
      stateToUpdate.confirmPassword = enteredConfirmPassword;
      stateToUpdate.confirmPasswordError = "";
      stateToUpdate.PasswordsMatched = true;
    }

    if (isBoxCheckValue) {
      stateToUpdate.TncError = "";
      stateToUpdate.TermsAccepted = true;
    } else {
      stateToUpdate.TncError = "Please accept terms and conditions";
    }

    this.setState(stateToUpdate);
  };

  getRequiredFields = () => {
    let {
      TermsAccepted,
      FirstName,
      LastName,
      Age,
      Gender,
      Email,
      Role,
      Password,
      confirmPassword,
      enteredPassword,
      enteredConfirmPassword,
    } = this.state;

    if (
      enteredPassword === enteredConfirmPassword &&
      FirstName &&
      LastName &&
      Age &&
      Gender &&
      Role &&
      Email &&
      Password &&
      confirmPassword &&
      TermsAccepted
    ) {
      console.log(`{firstName:${FirstName},lastName:${LastName},Age:${Age},
      Gender:${Gender},Role:${Role},Email:${Email}}`);

      this.setState({
        isDisabled: true,
        successMessage: "Account Created Successfully",
        FirstName: "",
        LastName: "",
        Age: "",
        Gender: "",
        Role: "",
        Email: "",
        Password: "",
        confirmPassword: "",
        CheckBox: false,
        FirstNameObtained: false,
        LastNameObtained: false,
        AgeObtained: false,
        GenderObtained: false,
        RoleObtained: false,
        EmailObtained: false,
        PasswordMetRules: false,
        PasswordsMatched: false,
        TermsAccepted: false,
      });
    }
  };

  isSubmitted = (event) => {
    event.preventDefault();
    this.validateData();
    this.getRequiredFields();
  };

  isBoxChecked = (event) => {
    this.setState({
      CheckBox: event.target.checked,
    });
  };

  isChanged = (event) => {
    let { name, value } = event.target;

    this.setState({
      [name]: value,
    });
  };

  render() {
    return (
      <div className="formTopMostContainer">
        <form className="formMainContainer" noValidate="noValidate" onSubmit={this.isSubmitted}>
          <div className="firstNameCon">
            <i className="fa-solid fa-user formIcon"></i>
            <input
              type="text"
              placeholder="First Name"
              className="FirstName"
              name="FirstName"
              value={this.state.FirstName}
              onChange={this.isChanged}
            ></input>
          </div>
          <p className="errorMessage">{this.state.FirstNameError}</p>
          <div className="lastNameCon">
            <i className="fa-solid fa-user formIcon"></i>
            <input
              type="text"
              placeholder="Last Name"
              className="lastName"
              name="LastName"
              value={this.state.LastName}
              onChange={this.isChanged}
            ></input>
          </div>
          <p className="errorMessage">{this.state.LastNameError}</p>
          <div className="ageCon">
            <i className="fa-solid fa-2 formIcon"></i>
            <input
              type="text"
              placeholder="Age"
              className="age"
              name="Age"
              value={this.state.Age}
              onChange={this.isChanged}
            ></input>
          </div>
          <p className="errorMessage">{this.state.AgeError}</p>
          <div className="GenderCon">
            <i className="fa-solid fa-mars-and-venus formIcon"></i>
            <label className="genderLabel">Gender</label>
            <select
              className="gender"
              name="Gender"
              value={this.state.Gender}
              onChange={this.isChanged}
            >
              <option>Select Gender </option>
              <option>Male</option>
              <option>Female</option>
              <option>Prefer Not to Say</option>
            </select>
          </div>
          <p className="errorMessage">{this.state.GenderError}</p>
          <div className="roleCon">
            <i className="fa-solid fa-laptop-code formIcon"></i>
            <label className="roleLabel">Role</label>
            <select
              className="role"
              name="Role"
              value={this.state.Role}
              onChange={this.isChanged}
            >
              <option>Select Role</option>
              <option>Devoleper</option>
              <option>Senior Devoleper</option>
              <option>Lead Devoleper</option>
              <option>CTO</option>
            </select>
          </div>
          <p className="errorMessage">{this.state.RoleError}</p>
          <div className="EmailCon">
            <i className="fa-solid fa-envelope formIcon"></i>
            <input
              type="email"
              className="emailId"
              placeholder="E-mail"
              name="Email"
              value={this.state.Email}
              onChange={this.isChanged}
            ></input>
          </div>
          <p className="errorMessage">{this.state.EmailError}</p>
          <div className="passwordCon">
            <i className="fa-solid fa-lock formIcon"></i>
            <input
              type="password"
              className="password"
              placeholder="Password"
              name="Password"
              value={this.state.Password}
              onChange={this.isChanged}
            ></input>
          </div>
          <p className="errorMessage">{this.state.PasswordError}</p>
          <div className="confPasswordCon">
            <i className="fa-solid fa-lock formIcon"></i>
            <input
              type="password"
              className="confPassword"
              placeholder="Confirm Password"
              name="confirmPassword"
              value={this.state.confirmPassword}
              onChange={this.isChanged}
            ></input>
          </div>
          <p className="errorMessage">{this.state.confirmPasswordError}</p>
          <div className="tncCon">
            <input
              type="checkbox"
              name="checkBox"
              id="checkBoxCheckId"
              value={this.state.CheckBox}
              onChange={this.isBoxChecked}
            ></input>
            <label htmlFor="checkBoxCheckId">
              I accept terms and conditions
            </label>
          </div>
          <p className="errorMessage">{this.state.TncError}</p>
          <div className="submitBtnCon">
            <button
              type="submit"
              className="submitBtn"
              disabled={this.state.isDisabled}
            >
              Submit
            </button>
          </div>
          <p className="passwordReqs">
            Password must be atleast 8 characters long with 1 lowercase letter,
            1 uppercase letter, 1 number, and a special character.
          </p>
          <h1 className="successMessage">{this.state.successMessage}</h1>
        </form>
      </div>
    );
  }
}

export default FormContent;
