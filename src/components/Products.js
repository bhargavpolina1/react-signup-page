import React, { Component } from "react";
import { Link } from "react-router-dom";
import ProductCard from "./ProductCard";
import "./Products.css";

class Products extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isDataFetched: false,
      FetchedData: "",
      isFetchingFailed: false,
    };
  }

  componentDidMount() {
    fetch("https://fakestoreapi.com/products")
      .then((res) => res.json())
      .then((data) => {
        this.setState({
          isDataFetched: true,
          FetchedData: data,
          isFetchingFailed: false,
        });
      })
      .catch((err) => {
        this.setState({
          isFetchingFailed: true,
        });
      });
  }

  render() {
    if (this.state.isDataFetched) {
      let fetchedLength = this.state.FetchedData.length;
      if (!fetchedLength) {
        return <div>No Products</div>;
      } else {
        return (
          <div className="container-fluid productsMainContainer">
            <div className="row">
              <div className="col-12 d-flex flex-wrap productCardContainer">
                {this.state.FetchedData.map((eachProduct) => {
                  return (
                    <Link
                      to={`/product/${eachProduct.id}`}
                      className="col-6 col-md-4 col-lg-3 mt-5"
                      key={eachProduct.id}
                    >
                      <ProductCard eachProduct={eachProduct} />
                    </Link>
                  );
                })}
              </div>
            </div>
          </div>
        );
      }
    } else if (this.state.isFetchingFailed) {
      return (
        <div className="errorContainer">
          <div className="errorImageContainer mt-5">
            <img
              className="errorImage"
              src="https://sitechecker.pro/wp-content/uploads/2017/12/404.png"
              alt="error"
            />
          </div>
          <p>There is an error while fetching data</p>
        </div>
      );
    } else {
      return (
        <div className="spinnerContainer">
          <div className="spinner-border text-primary" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        </div>
      );
    }
  }
}

export default Products;
