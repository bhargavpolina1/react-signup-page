import "./App.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import LandingPage from "./components/LandingPage";
import AboutUs from "./components/AboutUS";
import ContactUs from "./components/ContactUs";
import JoinUs from "./JoinUs";
import HeaderContent from "./components/Header";
import Products from "./components/Products";
import EachProduct from "./components/EachProduct";
function App() {
  return (
    <BrowserRouter>
    <HeaderContent/>
      <Switch>
        <Route exact path="/" component={LandingPage}/>
        <Route path="/products" component={Products}/>
        <Route path="/aboutus" component={AboutUs}/>
        <Route path="/contactus" component={ContactUs}/>
        <Route path="/signup" component={JoinUs}/>
        <Route exact path="/product/:id" render={(props) => <EachProduct  {...props} />} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
